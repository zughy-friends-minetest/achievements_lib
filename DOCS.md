# Achievements_lib Docs

# Table of Contents
* [1. Achievements](#1-achievements)
  * [1.1 Registering an achievement](#11-registering-an-achievement)
    * [1.1.1 Notificating players about new achievements](#111-notificating-players-about-new-achievements)
  * [1.2 Unregistering an achievement](#12-unregistering-an-achievement)
  * [1.3 Awarding and removing achievements](#13-awarding-and-removing-achievements)
  * [1.4 Getters](#14-getters)
  * [1.5 Utils](#15-utils)
  * [1.6 GUI](#16-gui)
* [2. About the author(s)](#2-about-the-authors)

## 1. Achievements
Achievements_lib is a library that handles achievements. It provides a system
for awarding achievements and viewing them, as well as a HUD notification on
award.

### 1.1 Registering an achievement

> **Beware**: achievements must be registered at load time or the mod will cause database issues. Do NOT add them at runtime!

`achvmt_lib.register_mod(mod_name, def)`: registers a mod that will contain achievements.
`def` is a table containing the following fields:
* `name`: (string) the name of the mod. Contrary to `mod_name`, this is not the technical name and it can be translated
* `icon`: (string) the icon representing the mod

`achvmt_lib.register_achievement(technical_name, achievement_def)`: registers an achievement. `technical_name` is a string that must follow the syntax `mod_name:whatever` (name must be unique). `achievement_def` is a table with the following fields:
* `title`: (string) the title of the achievement
* `description`: (string) the description of the achievement
* `additional_info`: (string) additional info about the achievement
* `image`: (string) the image of the achievement. Should be 26x26
* `tier`: (string) the tier of the string. Can be either `"Bronze"`, `"Silver"` or `"Gold"`
* `hidden`: (boolean) whether to hide the description until unlocked. `false` by default. If `true` and locked, a placeholder image and description will be put
* `on_award`: (function(p_name)): function run when awarded
* `on_unaward`: (function(p_name)): function run when unawarded

Example:

```lua
  {
    title = "Ultimate Bounciness",
    description = "Bounce 50 times",
    image = "mymod_ultimate_bounciness.png",
    tier = "Gold"
  }
  ```

After registration, when an achievement definition is returned, it will also contain the indexes `mod` and `name` for easy reference.

#### 1.1.1 Notificating players about new achievements

When one or more new achievements are added, achievements_lib keeps track of these changes. Comparing the last time players logged in with the time of such changes, achievements_lib is able to understand what are the achievements that a player hasn't seen yet; and, until "seen" through the functions below, they will treated as new every time they log in.  

By default, achievements_lib treats achievements as seen when the specific mod in the built-in GUI gets clicked. And, always by default, unseen achievements are displayed with a red dot - both on the item icon and on the GUI. Nonetheless, modders can set achievements as seen by using:  

* `achvmt_lib.mark_as_seen(p_name, ach)`: sets `ach` to seen for `p_name`
* `achvmt_lib.mark_as_seen_group(p_name, achs)`: sets a group of achievements (format `{"name1", "name2"}`) as seen for `p_name`
* `achvmt_lib.mark_as_seen_all(p_name)`: sets all the achievements of `p_name` as seen

> Beware: removing an achievement to then re-add it will cause every player that hasn't unlocked it yet to see such achievement as unseen - no matter if they actually had seen it before. Remove and re-add achievements sparingly!

### 1.2 Unregistering an achievement

> **Beware**: achievements must be unregistered at load time or the mod will cause database issues. Do NOT remove them at runtime!

Achievements can also be unregistered and there are two ways to do that:
* from the very mod of the achievement: delete the `register_achievement` call
* from an external mod: call `achvmt_lib.unregister_achievement(name)`. Useful when there are unwanted achievements you want to get rid of

If players have already earned an achievement that doesn't exist anymore, it'll remain dormant in their database. Meaning that, if the achievement is registered again, players won't have to unlock it anew.

### 1.3 Awarding and removing achievements

* `achvmt_lib.award(p_name, ach_name)`: awards an achievement to `p_name`. If the player is not online, then they will receive a hud popup when they next log on. Returns `true` on success and a string communicating whether the call succeeded
* `achvmt_lib.unaward(p_name, ach_name)`: removes an achievement from `p_name`. Returns `true` on success and a string communicating whether the call succeeded
* `achvmt_lib.unaward_all(p_name)`: same as `unaward` but for every achievement

**Note**: if a player is awarded with an achievement of a certain mod and then such mod gets disabled, the player won't lose their achievements. They'll simply remain silent until the mod is enabled again. This also means that functions like `get_player_achievements(..)` won't return the achievements belonging to mods that are currently disabled.

### 1.4 Getters

Some getters take an optional `filter` parameter. `filter` is a table to filter the output and it's structured as such:
```lua
  {
    mod = "My mod", -- modname
    tier = "Bronze", -- tier name
    hidden = false
  }
```

* `achvmt_lib.get_achievements(<filter>)`: returns a table of achievements, with names as key and def as value (`{ach_name1 = def1, ach_name2 = def2}`)
* `achvmt_lib.get_achvmt_by_name(ach_name)`: returns the definition table of `ach_name`
* `achvmt_lib.get_amount(<filter>)`: returns the amount of achievements. Currently `filter` only supports `mod`
* `achvmt_lib.get_player_achievements(p_name, <filter>)`: returns a table of achievements unlocked by `p_name`, with the achievement name as value (`{"ach_name1", "ach_name2"})
  * It returns `nil` if achievements_lib has never seen that player
* `achvmt_lib.get_player_unseen(p_name)`: returns the unseen achievements of `p_name` as a table. Format `{"ach_name1", "ach_name2"}`
  * It returns `nil` if achievements_lib has never seen that player
* `achvmt_lib.get_latest_unlocked(p_name, amount)`: returns a table with the latest `amount` achievements unlocked by `p_name`, starting from the newest to the oldest
  * It returns `nil` if achievements_lib has never seen that player
* `achvmt_lib.get_mods()`: returns a table with registered mod names as a value
* `achvmt_lib.get_mod_info(mod)`: returns a table containing the information provided when the mod was registered. `nil` if it doesn't exist


### 1.5 Utils

* `achvmt_lib.has_achievement(p_name, ach_name)`: returns whether the player has the specified achievement
* `achvmt_lib.exists(ach_name)`: returns whether `ach_name` exists


### 1.6 GUI

* `achvmt_lib.hud_show(p_name, achievement_def)`: shows a HUD popup to `p_name` when they are awarded an achievement. Override this function to change the default behaviour
* `achvmt_lib.gui_show(p_name, to_p_name)` shows `to_p_name` a menu with a scrollable list of `p_name` achievements. If `to_p_name` is not specified, it'll default to `p_name`. Override this function to implement custom behaviour.


## 2. About the author(s)
I'm Zughy (Marco), a professional Italian pixel artist who fights for free software and digital ethics. If this library spared you a lot of time and you want to support me somehow, please consider donating on [Liberapay](https://liberapay.com/Zughy/). I also want to thank MisterE for the initial help
