local version = "0.7.0-dev"
local modpath = minetest.get_modpath("achievements_lib")
local srcpath = modpath .. "/src"

-- TODO: remove after https://github.com/minetest/modtools/issues/2
local S = minetest.get_translator("achievements_lib")

S("Add unlockable achievements to your games and mods")

achvmt_lib = {}

dofile(modpath .. "/SETTINGS.lua")

dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/gui.lua")
dofile(srcpath .. "/hud.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")

if achvmt_lib.DEBUG_MODE then
    dofile(srcpath .. "/tests.lua")
end

minetest.log("action", "[ACHIEVEMENTS_LIB] Mod initialised, running version " .. version)
