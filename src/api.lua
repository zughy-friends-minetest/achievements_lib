local S = minetest.get_translator("achievements_lib")
local storage = minetest.get_mod_storage("achievements_lib")

----------------------------------------------
-----------      Init tables     -------------
----------------------------------------------

local achievements = {}                 -- KEY: ach_name; VALUE: {properties}
local updates = {}                      -- KEY: ach_name; VALUE: {(int) timestamp, (table) changes = {new1 = true, new2 = true}}
local mods = {}                         -- KEY: mod_name; VALUE: name, icon

local p_achievements = {}               -- KEY: p_name; VALUE: {ach_name1, ach_name2}
local p_unseen = {}                     -- KEY: p_name; VALUE: {ach_name1 = true, ach_name2 = true} -- new achievements the player hasn't interacted with yet
local player_ach_backlog = {}           -- KEY: p_name; VALUE: {1 = ach_name1, 2 = ach_name2} -- to temporarily track the order of achievements unlocked whilst offline

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

-- storage
local function store_player_achievements() end
local function recall_player_achievements() end

-- backlog to show hud popups when player logs on if they have not seen the
-- popups yet (awarded while offline; maybe team challenges were met, or they
-- won a tournament or something)
local function store_achievement_backlog() end
local function recall_achievement_backlog() end

local function store_player_unseen() end
local function recall_player_unseen() end

local function try_to_load_player() end
local function update_ach_icon() end





minetest.register_on_mods_loaded(function()
  local ach_names = {}
  for k, _ in pairs(achievements) do
    ach_names[k] = true
  end

  if storage:get_string("achvmts-list") ~= "" then
    local prev_achvmts = minetest.deserialize(storage:get_string("achvmts-list"))
    local new_ones = table.copy(ach_names)
    local prev_ones = table.copy(prev_achvmts)

    for k, _ in pairs(prev_achvmts) do
      new_ones[k] = nil
    end

    -- achvmts-list è da aggiornare anche in caso ora ce ne siano meno
    for k, _ in pairs(ach_names) do
      prev_ones[k] = nil
    end

    if next(new_ones) then
      updates = minetest.deserialize(storage:get_string("updates"))

      table.insert(updates, {timestamp = os.time(), changes = new_ones})
      storage:set_string("achvmts-list", minetest.serialize(ach_names))
      storage:set_string("updates", minetest.serialize(updates))

    elseif next(prev_ones) then
      storage:set_string("achvmts-list", minetest.serialize(ach_names))
    end

  else
    storage:set_string("achvmts-list", minetest.serialize(ach_names))
    storage:set_string("updates", minetest.serialize({}))
  end
end)



-- INTERNAL USE ONLY
function achvmt_lib.load_achievements(p_name, last_login)
  recall_player_achievements(p_name)
  recall_player_unseen(p_name)

  if not p_achievements[p_name] then
    p_achievements[p_name] = {}
    p_unseen[p_name] = {}
    player_ach_backlog[p_name] = {}

    store_player_achievements(p_name)
    store_player_unseen(p_name)

  else
    recall_achievement_backlog(p_name)

    local count = 0

    -- assegno eventuali prestigi sbloccati mentre non era connessə
    for _, ach in pairs(player_ach_backlog[p_name]) do
      -- 5 perché è il tempo che impiega l'avviso del prestigio a sparire, 0.1 per
      -- evitare che si sovrapponga con panel:hide()
      minetest.after(count * 5.1, function()
        if not minetest.get_player_by_name(p_name) then return end
        achvmt_lib.hud_show(p_name, achievements[ach])
      end)
      count = count + 1
    end

    player_ach_backlog[p_name] = {}

    -- controllo se ci son nuovi prestigi e li aggiungo in p_unseen
    if last_login and next(updates) and updates[#updates].timestamp >= last_login then
      for i = #updates, 1, -1 do
        if updates[i].timestamp <= last_login then
          break
        else
          for name, _ in pairs(updates[i].changes) do
            if achievements[name] and not achvmt_lib.has_achievement(p_name, name) then
              p_unseen[p_name][name] = true
            end
          end
        end
      end

      store_player_unseen(p_name)
    end

    local unseen_and_removed = false

    -- se quelli che aveva di non visti sono stati rimossi, rimuovili dai non visti
    for ach, _ in pairs(p_unseen[p_name]) do
      if not achievements[ach] then
        p_unseen[p_name][ach] = nil
        unseen_and_removed = true
      end
    end

    if unseen_and_removed then
      store_player_unseen(p_name)
    end

    -- cambia grafica oggetto nell'inventario
    if next(p_unseen[p_name]) then
      minetest.after(0, function() -- after in caso ci sian mod che sovrascrivono inventario all'accesso (es. Hub)
        if not minetest.get_player_by_name(p_name) then return end
        update_ach_icon(p_name, next(p_unseen[p_name]))
      end)

    elseif unseen_and_removed then
      minetest.after(0, function() update_ach_icon(p_name) end)
    end
  end

  store_achievement_backlog(p_name)
end



----------------------------------------------
----------------    API     ------------------
----------------------------------------------

function achvmt_lib.register_mod(mod, def)
  assert(not mods[mod], "A minigame can't be registered twice!")
  mods[mod] = {}

  local mod_ref = mods[mod]

  mod_ref.name = def.name
  mod_ref.icon = def.icon or "no_texture.png"
  mod_ref.ach_amount = 0 -- TODO rendila una tabella così da registare anche per leghe
  -- TODO considera se spostare qui le leghe per personalizzarle, es. {"Legno", "Bronzo", "Argento", "Platino"}
end



function achvmt_lib.register_achievement(name, def)
  local splitter_pos = string.find(name, ":")
  assert(splitter_pos, "[Achievements_lib] Achievement " .. name .. " doesn't follow the syntax modname:achname!")

  local mod = name:sub(1, splitter_pos -1)
  local mod_ref = mods[mod]
  assert(mod_ref, "[Achievements_lib] There is no registered mod " .. mod .. "!")
  assert(def.title, "[Achievements_lib] Title is mandatory!")

  local ach = {}

  -- for easy cross-reference
  ach.mod = mod
  ach.name = name
  -- all the rest
  ach.title = def.title
  ach.description = def.description or ""
  ach.image = def.image or "achievements_lib_item.png"
  ach.tier = def.tier
  ach.hidden = def.hidden or false
  ach.additional_info = def.additional_info or ""
  ach.on_award = def.on_award or function(p_name) return end
  ach.on_unaward = def.on_unaward or function(p_name) return end

  mod_ref.ach_amount = mod_ref.ach_amount + 1
  achievements[name] = ach
end



function achvmt_lib.unregister_achievement(name)
  local splitter_pos = string.find(name, ":")
  assert(splitter_pos, "[Achievements_lib] Achievement " .. name .. " doesn't follow the syntax modname:achname!")

  local mod = name:sub(1, splitter_pos -1)
  local mod_ref = mods[mod]

  if not mod_ref then
    minetest.log("action", "[Achievements_lib] Trying to unregister an achievement of a mod that doesn't exist (" .. name .. ")")
    return end

  if not achievements[name] then
    minetest.log("action", "[Achievements_lib] Trying to unregister an achievement that doesn't exist (" .. name .. ")")
    return end

  mod_ref.ach_amount = mod_ref.ach_amount - 1
  achievements[name] = nil
end



function achvmt_lib.award(p_name, ach_name)
  -- can't award to a player that does not exist
  local player_exists, is_offline = try_to_load_player(p_name)

  if not player_exists then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] Can't find any player called @1 in the mod database!", p_name)) end

  if not achvmt_lib.exists(ach_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] No achievement by the name @1!", ach_name)) end

  if achvmt_lib.has_achievement(p_name, ach_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] @1 already has achievement @2!", p_name, ach_name)) end

  -- give achievement and save it
  table.insert(p_achievements[p_name], ach_name)

  local ach_def = achievements[ach_name]

  -- store popup backlog if player is offline, or show popup now if they are
  -- online
  if is_offline then
    table.insert(player_ach_backlog[p_name], ach_name)
    store_achievement_backlog(p_name)
  else
    achvmt_lib.hud_show(p_name, ach_def)
  end

  ach_def.on_award(p_name)

  store_player_achievements(p_name)
  return true, "[achvmt_lib] " .. S("Achievement @1 awarded to @2", ach_name, p_name)
end



function achvmt_lib.unaward(p_name, ach_name)
  if not try_to_load_player(p_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] Can't find any player called @1 in the mod database!", p_name)) end

  if not achvmt_lib.exists(ach_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] No achievement by the name @1!", ach_name)) end

  if not achvmt_lib.has_achievement(p_name, ach_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] @1 has no achievement called @2!", p_name, ach_name)) end

  achievements[ach_name].on_unaward(p_name)

  for i, name in ipairs(p_achievements[p_name]) do
    if ach_name == name then
      table.remove(p_achievements[p_name], i)
      break
    end
  end

  store_player_achievements(p_name)
  return true, "[ACHVMT_LIB] " .. S("Achievement @1 removed from @2", ach_name, p_name)
end



function achvmt_lib.unaward_all(p_name)
  if not try_to_load_player(p_name) then
    return false, minetest.colorize("#e6482e", "[ACHVMT_LIB] " .. S("[!] Can't find any player called @1 in the mod database!", p_name)) end

  for _, ach_name in pairs(p_achievements[p_name]) do
    if achievements[ach_name] then
      achievements[ach_name].on_unaward(p_name)
    end
  end

  p_achievements[p_name] = {}
  store_player_achievements(p_name)

  return true, "[ACHVMT_LIB] " .. S("All achievements removed from @1", p_name)
end



function achvmt_lib.mark_as_seen(p_name, ach)
  p_unseen[p_name][ach] = nil

  store_player_unseen(p_name)

  if not next(p_unseen[p_name]) then
    update_ach_icon(p_name)
  end
end



function achvmt_lib.mark_as_seen_group(p_name, achs)
  for _, ach_name in pairs(achs) do
    p_unseen[p_name][ach_name] = nil
  end

  store_player_unseen(p_name)

  if not next(p_unseen[p_name]) then
    update_ach_icon(p_name)
  end
end



function achvmt_lib.mark_as_seen_all(p_name)
  p_unseen[p_name] = {}
  store_player_unseen(p_name)
  update_ach_icon(p_name)
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function achvmt_lib.has_achievement(p_name, ach_name)
  if not try_to_load_player(p_name) then return end

  for _, ach in pairs(p_achievements[p_name]) do
    if ach == ach_name then
      return true
    end
  end
end



function achvmt_lib.exists(ach_name)
  return achievements[ach_name] ~= nil
end





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function achvmt_lib.get_mods()
  local ach_mods = {}
  for mod, _ in pairs(mods) do
    table.insert(ach_mods, mod)
  end
  return ach_mods
end



function achvmt_lib.get_mod_info(mod_name)
  return mods[mod_name]
end



function achvmt_lib.get_achievements(filter)
  if not filter then return achievements end

  local achs = {}

  for name, ach_def in pairs(achievements) do
    local include = true

    if filter.mod and ach_def.mod ~= filter.mod then
      include = false
    end
    if filter.tier and ach_def.tier ~= filter.tier then
      include = false
    end
    if filter.hidden and ach_def.hidden ~= filter.hidden then
      include = false
    end

    if include then
      achs[name] = ach_def
    end
  end

  return achs
end



function achvmt_lib.get_amount(filter)
  local amount = 0

  for _, data in pairs(mods) do
    amount = amount + data.ach_amount
  end

  if not filter then
    return amount

  else
    for _, ach_def in pairs(achievements) do
      if filter.mod and ach_def.mod ~= filter.mod or
         filter.tier and ach_def.tier ~= filter.tier or
         filter.hidden and ach_def.hidden ~= filter.hidden then
        amount = amount -1
      end
    end

    return amount
  end
end



function achvmt_lib.get_achvmt_by_name(ach_name)
  return achievements[ach_name]
end



function achvmt_lib.get_player_achievements(p_name, filter)
  if not try_to_load_player(p_name) then return end

  local achs = {}

  if not filter then
    achs = table.copy(p_achievements[p_name])
    for i, ach_name in pairs(p_achievements[p_name]) do
      if not achvmt_lib.exists(ach_name) then
        achs[i] = nil
      end
    end

  else
    for _, ach_name in pairs(p_achievements[p_name]) do
      local ach_def = achievements[ach_name]

      if achvmt_lib.exists(ach_name) then
        if ach_def then
          local include = true

          if filter.mod and ach_def.mod ~= filter.mod then
            include = false
          end

          if filter.tier and ach_def.tier ~= filter.tier then
            include = false
          end

          if filter.hidden and ach_def.hidden ~= filter.hidden then
            include = false
          end

          if include then
            table.insert(achs, ach_name)
          end
        end
      end
    end
  end

  return achs
end



function achvmt_lib.get_player_unseen(p_name)
  if not try_to_load_player(p_name) then return end
  return p_unseen[p_name]
end



function achvmt_lib.get_latest_unlocked(p_name, amount)
  if not try_to_load_player(p_name) then return end

  local latest = {}

  for i = 1, math.min(amount, #p_achievements[p_name]) do
    table.insert(latest, p_achievements[p_name][i])
  end

  return latest
end





----------------------------------------------
----------- Local funcs defined --------------
----------------------------------------------

function store_player_achievements(p_name)
  local ach = minetest.serialize(p_achievements[p_name])

  storage:set_string("achvmts_" .. p_name, ach)
end



function recall_player_achievements(p_name)
  local ach = storage:get_string("achvmts_" .. p_name)

  if ach == "" then return end

  p_achievements[p_name] = minetest.deserialize(ach)
end



function store_achievement_backlog(p_name)
  local bck = minetest.serialize(player_ach_backlog[p_name])
  storage:set_string("backlog_" .. p_name, bck)
end



function recall_achievement_backlog(p_name)
  local bck = storage:get_string("backlog_" .. p_name)

  if bck == "" then return end

  player_ach_backlog[p_name] = minetest.deserialize(bck)
end



function store_player_unseen(p_name)
  storage:set_string("unseen_" .. p_name, minetest.serialize(p_unseen[p_name]))
end



function recall_player_unseen(p_name)
  local unseen_ach = storage:get_string("unseen_" .. p_name)

  -- ritorna tab vuota perché inserito postumo - ergo alcunɜ giocanti potrebbero avere
  -- i prestigi ma non esistere in p_unseen
  p_unseen[p_name] = unseen_ach == "" and {} or minetest.deserialize(unseen_ach)
end



function try_to_load_player(p_name)
  local player_exists = true
  local is_offline = minetest.get_player_by_name(p_name) == nil

  if not p_achievements[p_name] then
    recall_player_achievements(p_name)
    recall_player_unseen(p_name)
    recall_achievement_backlog(p_name)

    player_exists = p_achievements[p_name] ~= nil
  end

  return player_exists, is_offline
end



function update_ach_icon(p_name, as_unseen)
  local p_inv = minetest.get_player_by_name(p_name):get_inventory()
  local texture = as_unseen and "achievements_lib_item_new.png" or "achievements_lib_item.png"

  for list, _ in pairs(p_inv:get_lists()) do
    if p_inv:contains_item(list, "achievements_lib:achievements_menu") then
      for i = 1, p_inv:get_size(list) do
        if p_inv:get_stack(list, i):get_name() == "achievements_lib:achievements_menu" then
          local ach_item = ItemStack("achievements_lib:achievements_menu")

          ach_item:get_meta():set_string("inventory_image", texture)
          p_inv:set_stack(list, i, ach_item)
        end
      end
    end
  end
end