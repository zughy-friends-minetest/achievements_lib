local S = minetest.get_translator("achievements_lib")

local player_scroll_indexes = {}
local players_viewing = {} -- whose acheivements each player is viewing
local player_tabs = {}
local hide_achieved = {}        -- KEY: p_name, VALUE: boolean
local first_visible_tab = {} -- first visible tab for each player

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

local function make_scrollbaroptions_for_scroll_container() end
local function get_tabs() end
local function sort_ach() end

-- Makes and returns the achivement list that goes in the scroll container. Also
-- returns the formspec-length of the list for the scrollbar options. @p_name is
-- the player who has the achievments, @to_p_name is the player to whom the
-- achievements are shown, defaults to p_name. @filter is a filter table as
-- detailed in DOCS.md, and @hide states whether to hide unlocked achievements
local function get_achievement_list_fs() end

-- returns table of available mod tabnames
local function get_available_tabs() end


----------------------------------------------
----------------    API     ------------------
----------------------------------------------

function achvmt_lib.gui_show(p_name, to_p_name)
  to_p_name = to_p_name or p_name
  players_viewing[to_p_name] = p_name -- keep track of which achievements fs to_p_name is viewing
  player_tabs[to_p_name] = player_tabs[to_p_name] or "ALL" -- the tab name for each player

  local tot_all_ach = achvmt_lib.get_amount()
  local filter = {}
  local hide_unlocked = hide_achieved[to_p_name]

  -- filter by mod if in a mod tab
  if player_tabs[to_p_name] ~= "ALL" then
    filter.mod = player_tabs[to_p_name]
    tot_all_ach = achvmt_lib.get_amount({mod = filter.mod})
  end

  local tot_achieved_ach = #achvmt_lib.get_player_achievements(p_name, filter)
  local ach_list, total_l = get_achievement_list_fs(p_name, to_p_name, filter, hide_unlocked)
  local visible_l = 5.875
  local unach_only = hide_unlocked and "achievements_lib_fs_filter_on.png" or "achievements_lib_fs_filter_off.png"
  local scrollbar = ""

  player_scroll_indexes[to_p_name] = player_scroll_indexes[to_p_name] or 0

  if total_l >= visible_l then
    scrollbar = make_scrollbaroptions_for_scroll_container(visible_l, total_l, .1, "hide") ..
                "scrollbar[12.8125,2.625;.125," .. visible_l .. ";vertical;ACH_SCROLL;" .. player_scroll_indexes[to_p_name] .. "]"
  end

  local fs = {
    "formspec_version[6]",
    "size[15,10]",
    "padding[.01,.01]",
    "no_prepend[]",
    "bgcolor[;neither;]",
    "background[0,0;15,10;achievements_lib_fs_bg.png]",
    "hypertext[2,1;11,1;title;<global size=22 halign=center valign=middle><style color=#cfc6b8><b>" .. S("@1's achievements", p_name) .. "</b>]",
    "hypertext[2,2;11,0.6;amount;<global size=18 halign=center valign=middle><style color=#cfc6b8><b>" .. tot_achieved_ach .. "/" .. tot_all_ach .. "</b>]",
    "style[P_NAME;border=false;font=bold;font_size=*.9]",
    "style_type[image_button;border=false]",
    "image_button[13.81,0.71;0.5,0.5;achievements_lib_fs_close.png;close;]",
    "image_button[0.93,2.97;0.8,0.8;" .. unach_only .. ";HASNT;]",
    "tooltip[0.93,2.97;0.8,0.8;" .. S("Filter by Unachieved") .. ";#7a444a;#f4cca1]",
    "scroll_container[2.14,2.625;10.8125," .. visible_l .. ";ACH_SCROLL;vertical;.1]",
    ach_list or "",
    "scroll_container_end[]",
    scrollbar,
    get_tabs(p_name, to_p_name)
  }

  minetest.show_formspec(to_p_name, "achvmt_lib_disp", table.concat(fs, ""))
end

----------------------------------------------
-------------  Functions defined -------------
----------------------------------------------

-- This Function MIT by Rubenwardy
--- Creates a scrollbaroptions for a scroll_container
--
-- @param visible_l the length of the scroll_container and scrollbar
-- @param total_l length of the scrollable area
-- @param scroll_factor as passed to scroll_container
function make_scrollbaroptions_for_scroll_container(visible_l, total_l, scroll_factor, arrows)
  assert(total_l >= visible_l)
  arrows = arrows or "default"
  local thumb_size = (visible_l / total_l) * (total_l - visible_l)
  local max = total_l - visible_l

  return ("scrollbaroptions[min=0;max=%f;thumbsize=%f;arrows=%s]"):format(max / scroll_factor, thumb_size / scroll_factor, arrows)
end



function get_achievement_list_fs(p_name, to_p_name, filter, hide_unlocked)
  local all_achievements = achvmt_lib.get_achievements(filter)

  local ach_list = {}

  if hide_unlocked then
    for ach_name, ach in pairs(all_achievements) do
      if not achvmt_lib.has_achievement(to_p_name, ach_name) then
        table.insert(ach_list, ach)
      end
    end

  else
    for _, ach in pairs(all_achievements) do
      table.insert(ach_list, ach)
    end
  end

  -- tier > hidden > name
  table.sort(ach_list, function(a, b) return sort_ach(a,b, p_name) end)

  local fs = {}
  local count = 0
  local list_height_multiplier = 1.5
  local total_height = 0

  if not next(ach_list) then
    local txt
    local tab = player_tabs[to_p_name]

    if not hide_unlocked or achvmt_lib.get_amount(filter) == 0 then
      txt = S("No achievements ... yet")
    else
      if tab == "ALL" and #achvmt_lib.get_player_achievements(p_name) == achvmt_lib.get_amount() then
        txt = S("You've got all the achievements!")
      else
        txt = S("You've got all the achievements of this minigame!")
      end
    end

    fs = {
      "image[0,0;10.75," .. list_height_multiplier .. ";achievements_lib_fs_unachlist_bg.png]",
      "style[INFO;border=false;textcolor=#dff6f5;font=italic;font_size=*1]",
      "button[0,0;10.75," .. list_height_multiplier .. ";INFO;" .. txt .. "]"
    }

    total_height = list_height_multiplier

  else
    local p_unseen = achvmt_lib.get_player_unseen(p_name)

    -- build the ach list fs from the ach_list
    for _, ach in ipairs(ach_list) do
      local ach_name = ach.name
      local has_ach = achvmt_lib.has_achievement(to_p_name, ach_name)
      local mod = achvmt_lib.get_mod_info(ach.mod)
      local bg_img = "achievements_lib_fs_bg_" .. string.lower(ach.tier) -- alternate bg colors in list
      local y_pos = count * list_height_multiplier
      local ach_image = ach.image
      local ach_title = ach.title
      local ach_description = ach.description
      local unseen_icon = p_unseen[ach_name] and "image[10.1," .. (y_pos + 0.1) .. ";0.5,0.5;achievements_lib_unseen_icon.png]" or ""
      local grey_out

      if has_ach then
        grey_out = ""
      else
        if hide_achieved[p_name] then
          grey_out = "box[0," .. (y_pos - .02) .. ";10.75," .. (list_height_multiplier + 0.02) .. ";#22222277]"
        else
          grey_out = "box[0," .. (y_pos - .02) .. ";10.75," .. (list_height_multiplier + 0.02) .. ";#2222228d]"
        end
      end


      if ach.hidden and not has_ach then
        ach_title = "???"
        ach_image = "achievements_lib_hidden.png"
        ach_description = "???" .. " "
      end

      total_height = total_height + list_height_multiplier

      local ach_fs = {
        "image[0," .. y_pos .. ";10.75," .. list_height_multiplier .. ";" .. bg_img .. ".png]",
        "image[0.2," .. (y_pos + 0.1) .. ";1.3,1.3;" .. ach_image .. "]",
        "image[9.8," .. (y_pos + 0.4) .. ";0.65,0.65;" .. mod.icon .. "]",
        "tooltip[9.8," .. (y_pos + 0.4) .. ";0.65,0.65;" .. mod.name .. ";#7a444a;#f4cca1]",
        "style_type[label;border=false;textcolor=#472d3c;font=bold;font_size=*1.4]",
        "label[1.8," .. (y_pos + 0.45) .. ";" .. ach_title .. "]",
        "style_type[label;border=false;textcolor=#472d3c;font=italic;font_size=*0.9]",
        "label[1.8," .. (y_pos + 1) .. ";" .. ach_description .. "]",
        "tooltip[0," .. y_pos .. ";9.8,".. list_height_multiplier .. ";".. ach.additional_info ..";#7a444a;#f4cca1]",
        grey_out,
        unseen_icon
        }

      for _, v in pairs(ach_fs) do
        table.insert(fs, v)
      end

      count = count + 1
    end
  end

  return table.concat(fs, ""), total_height
end



function get_tabs(p_name, to_p_name)
  local max_visible_tabs = 8
  local available_tabs = get_available_tabs()
  first_visible_tab[to_p_name] = first_visible_tab[to_p_name] or 1

  local fs = {}
  local visible_tabs = {}

  for i = first_visible_tab[to_p_name], first_visible_tab[to_p_name] + max_visible_tabs do
    if available_tabs[i] then
      table.insert(visible_tabs, available_tabs[i])
    end
  end

  local p_unseen = achvmt_lib.get_player_unseen(p_name)

  for i, tabname in ipairs(visible_tabs) do -- tabname is modname or "ALL"
    local x = 2.8 + (i - 1) * 1.05
    local unlocked_ach, tot_ach, texture, tooltip, unseen

    if tabname == "ALL" then
      unlocked_ach = #achvmt_lib.get_player_achievements(p_name)
      tot_ach = achvmt_lib.get_amount()
      texture = "achievements_lib_item.png"
      tooltip = S("All Achievements")
      unseen = ""
    else
      local mod = achvmt_lib.get_mod_info(tabname)

      unlocked_ach = #achvmt_lib.get_player_achievements(p_name, {mod = tabname})
      tot_ach = achvmt_lib.get_amount({mod = tabname})
      texture = mod.icon
      tooltip = S("Achievements from") .. " " .. mod.name

      for ach, _ in pairs(achvmt_lib.get_achievements({mod = tabname})) do
        if p_unseen[ach] then
          unseen = "image[" .. x + 0.6 .. ", 8.56;0.35,0.35;achievements_lib_unseen_icon.png]"
          break
        end
      end
    end

    tooltip = tooltip .. " (" .. unlocked_ach .. "/" .. tot_ach .. ")"

    local bg
    local completed = tot_ach == unlocked_ach and ("image[" .. x + 0.6 .. ", 9.1;0.35,0.35;achvmtlib_fs_checkmark.png]") or ""

    if completed ~= "" then
      bg = player_tabs[to_p_name] == tabname and "achievements_lib_tab_complete_bg_selected.png" or "achievements_lib_tab_complete_bg.png"
    else
      bg = player_tabs[to_p_name] == tabname and "achievements_lib_tab_bg_selected.png" or "achievements_lib_tab_bg.png"
    end

    local tab_fs = {
      "style_type[image_button;border=false]",
      "image[" .. x .. ",8.58;0.97,0.97;" .. bg .. "]",
      "image_button[" .. x + .133 .. ",8.65;0.73,0.73;" .. texture .. ";" .. tabname .. ";]",
      "tooltip[" .. x .. ",8.58;0.97,0.97;" .. tooltip .. ";#7a444a;#f4cca1]",
      unseen,
      completed
    }

    for _, v in pairs(tab_fs) do
      table.insert(fs, v)
    end
  end

  -- show arrows
  if (#available_tabs > max_visible_tabs) and (first_visible_tab[to_p_name] ~= 1) then
    table.insert(fs, "image_button[" .. 32 / 16 .. "," .. 138 / 16 .. ";" .. 8 / 16 .. "," .. 14 / 16 .. ";achievements_lib_arrow_left.png;TAB_LEFT;]")
  end

  if (#available_tabs > max_visible_tabs) and (first_visible_tab[to_p_name] ~= #available_tabs - max_visible_tabs) then
    table.insert(fs, "image_button[" .. 201 / 16 .. "," .. 138 / 16 .. ";" .. 8 / 16 .. "," .. 14 / 16 .. ";achievements_lib_arrow_right.png;TAB_RIGHT;]")
  end

  return table.concat(fs, "")
end



function get_available_tabs()
  local available_tabs = {}
  table.insert(available_tabs, "ALL")
  for _, mod in pairs(achvmt_lib.get_mods()) do
    table.insert(available_tabs, mod)
  end

  return available_tabs
end



function sort_ach(a, b, p_name)
  if a.tier ~= b.tier then
    if a.tier == "Bronze" or b.tier == "Gold" then
      return true
    else
      return false
    end

  elseif tostring(a.hidden) ~= tostring(b.hidden) then
    return a.hidden == false

  else
    local lang = minetest.get_player_information(p_name).lang_code
    local a_title = minetest.get_translated_string(lang, a.title)
    local b_title = minetest.get_translated_string(lang, b.title)

    return a_title < b_title
  end
end





----------------------------------------------
-------------      Callback      -------------
----------------------------------------------

minetest.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "achvmt_lib_disp" then return end

  local p_name = player:get_player_name()

  if fields.close then
    minetest.close_formspec(p_name, "achvmt_lib_disp")
    return
  end

  if fields.HASNT then
    hide_achieved[p_name] = not hide_achieved[p_name]
    achvmt_lib.gui_show(players_viewing[p_name], p_name)
  end

  local available_tabs = get_available_tabs()

  -- mod tabs
  for _, tabname in ipairs(available_tabs) do
    if fields[tabname] then
      player_tabs[p_name] = tabname
      achvmt_lib.gui_show(players_viewing[p_name], p_name)

      local p_unseen = achvmt_lib.get_player_unseen(p_name)
      local unseen_achs = {}

      -- prima ciclo, poi fo una chiamata unica alla base dati
      for ach, _ in pairs(achvmt_lib.get_achievements({mod = tabname})) do
        if p_unseen[ach] then
          table.insert(unseen_achs, ach)
        end
      end

      if next(unseen_achs) then
        achvmt_lib.mark_as_seen_group(p_name, unseen_achs)
      end
    end
  end

  -- move mod tab arrows; buttons wont be shown if they are not supposed to
  -- be, so we dont have to check.
  if fields.TAB_LEFT then
    first_visible_tab[p_name] = first_visible_tab[p_name] - 1
    achvmt_lib.gui_show(players_viewing[p_name], p_name)

  elseif fields.TAB_RIGHT then
    first_visible_tab[p_name] = first_visible_tab[p_name] + 1
    achvmt_lib.gui_show(players_viewing[p_name], p_name)
  end
end)