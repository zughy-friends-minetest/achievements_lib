local S = minetest.get_translator("achievements_lib")

minetest.register_craftitem("achievements_lib:achievements_menu", {
  inventory_image = "achievements_lib_item.png",
  description = S("Achievements"),
  on_place = function() end,
  on_drop = function() end,


  on_use = function(itemstack, player, pointed_thing)
    if not player then return end
    achvmt_lib.gui_show(player:get_player_name())
  end
})



-- just for debugging, when taking the item from creative inventory
minetest.register_on_player_inventory_action(function(player, action, inventory, inventory_info)
  if action == "put" then
    local stack = inventory_info.stack

    if stack:get_name() ~= "achievements_lib:achievements_menu" then return end

    local p_name = player:get_player_name()
    local has_unseen = next(achvmt_lib.get_player_unseen(p_name))
    local i_meta = stack:get_meta()
    local stack_img = i_meta:get_string("inventory_image")

    if has_unseen and stack_img ~= "achievements_lib_item_new.png" then
      i_meta:set_string("inventory_image", "achievements_lib_item_new.png")
      inventory:set_stack(inventory_info.listname, inventory_info.index, stack)
    end
  end
end)